using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class SetDepthTextureMode : MonoBehaviour
{
    public bool EnableDepthTexture;

    void SetDepthTextureModeFunction()
    {
        Camera cam = GetComponent<Camera>();
        if (EnableDepthTexture)
            cam.depthTextureMode |= DepthTextureMode.Depth;
        else
            cam.depthTextureMode &= ~DepthTextureMode.Depth;
    }

    void OnEnable()
    {
        SetDepthTextureModeFunction();
    }

#if UNITY_EDITOR
    void OnValidate()
    {
        SetDepthTextureModeFunction();
    }
#endif
}
