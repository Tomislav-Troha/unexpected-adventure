using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubsTrigger : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && this.gameObject.tag == "subs_trigger")
        {
            //transform.parent.gameObject.GetComponent<EnemySpawner>().spawnSickHumans_Wave1();
            gameObject.GetComponent<BoxCollider>().enabled = false;

            //print(this.gameObject.transform);
            StartCoroutine(transform.parent.gameObject.GetComponent<Subtitles>().showSubs(this.gameObject.transform));
        }
    }
}
