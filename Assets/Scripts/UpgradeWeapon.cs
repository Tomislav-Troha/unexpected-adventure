using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeWeapon : MonoBehaviour
{
    private GameObject m4_carbine;
    private GameObject pistol;

    public int upgradeCostPistol;
    public int upgradeCostM4;

    public Text PistolcurrentLevel;
    public Text M4CarbinecurrentLevel;

    public Text txUpgradCostPistol;
    public Text txUpgradCostM4;


    private GameObject inventroyMetalScraps;



    private void Start()
    {

        inventroyMetalScraps = GameObject.FindWithTag("Player");


        inventroyMetalScraps.GetComponent<Inventory>().txMetalScraps.text = "Metal scraps: " + inventroyMetalScraps.GetComponent<Inventory>().metalScraps.ToString();

        m4_carbine = GameObject.Find("M4_Carbine");
        pistol = GameObject.Find("Pistol");
    }

    private void Update()
    {
        upgradingPistolLevels();
        upgradingM4CarbineLevels();

    }


    public void upgradePistol()
    {
        if (PistolcurrentLevel.text == "Level 1" && inventroyMetalScraps.GetComponent<Inventory>().metalScraps >= upgradeCostPistol)
        {

            inventroyMetalScraps.GetComponent<Inventory>().metalScraps -= upgradeCostPistol;
            inventroyMetalScraps.GetComponent<Inventory>().txMetalScraps.text = "Metal scraps: " + inventroyMetalScraps.GetComponent<Inventory>().metalScraps.ToString();

            pistol.GetComponent<GunSystem>().damage += 3;
            pistol.GetComponent<GunSystem>().range += 10;
            pistol.GetComponent<GunSystem>().magazineSize += 2;
        }

        if (PistolcurrentLevel.text == "Level 2" && inventroyMetalScraps.GetComponent<Inventory>().metalScraps >= upgradeCostPistol)
        {

            inventroyMetalScraps.GetComponent<Inventory>().metalScraps -= upgradeCostPistol;
            inventroyMetalScraps.GetComponent<Inventory>().txMetalScraps.text = "Metal scraps: " + inventroyMetalScraps.GetComponent<Inventory>().metalScraps.ToString();

            pistol.GetComponent<GunSystem>().damage += 3;
            pistol.GetComponent<GunSystem>().range += 10;
            pistol.GetComponent<GunSystem>().magazineSize += 2;
        }

        if (PistolcurrentLevel.text == "Level 3" && inventroyMetalScraps.GetComponent<Inventory>().metalScraps >= upgradeCostPistol)
        {

            inventroyMetalScraps.GetComponent<Inventory>().metalScraps -= upgradeCostPistol;
            inventroyMetalScraps.GetComponent<Inventory>().txMetalScraps.text = "Metal scraps: " + inventroyMetalScraps.GetComponent<Inventory>().metalScraps.ToString();

            pistol.GetComponent<GunSystem>().damage += 3;
            pistol.GetComponent<GunSystem>().range += 10;
            pistol.GetComponent<GunSystem>().magazineSize += 2;
        }


    }


    public void upgradeM4Carbine()
    {
        if (M4CarbinecurrentLevel.text == "Level 1" && inventroyMetalScraps.GetComponent<Inventory>().metalScraps >= upgradeCostM4)
        {
            inventroyMetalScraps.GetComponent<Inventory>().metalScraps -= upgradeCostM4;
            inventroyMetalScraps.GetComponent<Inventory>().txMetalScraps.text = "Metal scraps: " + inventroyMetalScraps.GetComponent<Inventory>().metalScraps.ToString();

            m4_carbine.GetComponent<GunSystem>().damage += 3;
            m4_carbine.GetComponent<GunSystem>().range += 10;
            m4_carbine.GetComponent<GunSystem>().magazineSize += 5;
        }

        if (M4CarbinecurrentLevel.text == "Level 2" && inventroyMetalScraps.GetComponent<Inventory>().metalScraps >= upgradeCostM4)
        {

            inventroyMetalScraps.GetComponent<Inventory>().metalScraps -= upgradeCostM4;
            inventroyMetalScraps.GetComponent<Inventory>().txMetalScraps.text = "Metal scraps: " + inventroyMetalScraps.GetComponent<Inventory>().metalScraps.ToString();

            m4_carbine.GetComponent<GunSystem>().damage += 3;
            m4_carbine.GetComponent<GunSystem>().range += 10;
            m4_carbine.GetComponent<GunSystem>().magazineSize += 5;
        }

        if (M4CarbinecurrentLevel.text == "Level 3" && inventroyMetalScraps.GetComponent<Inventory>().metalScraps >= upgradeCostM4)
        {

            inventroyMetalScraps.GetComponent<Inventory>().metalScraps -= upgradeCostM4;
            inventroyMetalScraps.GetComponent<Inventory>().txMetalScraps.text = "Metal scraps: " + inventroyMetalScraps.GetComponent<Inventory>().metalScraps.ToString();

            m4_carbine.GetComponent<GunSystem>().damage += 3;
            m4_carbine.GetComponent<GunSystem>().range += 10;
            m4_carbine.GetComponent<GunSystem>().magazineSize += 5;
        }


    }


    private void upgradingM4CarbineLevels()
    {
        if (M4CarbinecurrentLevel.text == "Level 1")
        {
            upgradeCostM4 = 70;
            txUpgradCostM4.text = upgradeCostM4.ToString();
        }

        if (m4_carbine.GetComponent<GunSystem>().damage == 28)
        {
            M4CarbinecurrentLevel.text = "Level 2";
            upgradeCostM4 = 90;
            txUpgradCostM4.text = upgradeCostM4.ToString();
        }

        if (m4_carbine.GetComponent<GunSystem>().damage == 31)
        {
            M4CarbinecurrentLevel.text = "Level 3";
            upgradeCostM4 = 110;
            txUpgradCostM4.text = upgradeCostM4.ToString();
        }

        if (m4_carbine.GetComponent<GunSystem>().damage == 34)
        {
            M4CarbinecurrentLevel.text = "Level 4";
            upgradeCostM4 = 130;
            txUpgradCostM4.text = "";
        }
    } 


    private void upgradingPistolLevels()
    {
        if (PistolcurrentLevel.text == "Level 1")
        {
            upgradeCostPistol = 50;
            txUpgradCostPistol.text = upgradeCostPistol.ToString();
        }

        if (pistol.GetComponent<GunSystem>().damage == 23)
        {
            PistolcurrentLevel.text = "Level 2";
            upgradeCostPistol = 70;
            txUpgradCostPistol.text = upgradeCostPistol.ToString();
        }

        if (pistol.GetComponent<GunSystem>().damage == 26)
        {
            PistolcurrentLevel.text = "Level 3";
            upgradeCostPistol = 90;
            txUpgradCostPistol.text = upgradeCostPistol.ToString();
        }

        if (pistol.GetComponent<GunSystem>().damage == 29)
        {
            PistolcurrentLevel.text = "Level 4";
            upgradeCostPistol = 110;
            txUpgradCostPistol.text = "";
        }
    }




}
