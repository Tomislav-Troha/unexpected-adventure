using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{

    private GameObject player;

    private GameObject m4_carbine;
    private GameObject pistol;
    private GameObject axe;

    public Texture icon;

    public string type;
    public float increaseRate;

    public Vector3 rotation;
    public Vector3 position;
    public Vector3 scale;

    public bool pickedUp;
    public bool equipped;

    private void Start()
    {
        m4_carbine = GameObject.Find("M4_Carbine");
        pistol = GameObject.Find("Pistol");
        axe = GameObject.Find("Axe");

        player = GameObject.FindWithTag("Player");
    }
    private void Update()
    {
        //ako nam je oruzje u ruci klikni g za unequipp
        if (equipped)
        {
            if (Input.GetKeyDown(KeyCode.G))
            {
                Unequip();
            }
        }
    }

    public void Unequip()
    {
        pistol.GetComponent<GunSystem>().textHolder.enabled = false;
        pistol.GetComponent<GunSystem>().enabled = false;

        m4_carbine.GetComponent<GunSystem>().enabled = false;
        m4_carbine.GetComponent<GunSystem>().textHolder.enabled = false;

        m4_carbine.GetComponent<GunSystem>().text.enabled = false;

        axe.GetComponent<MeleeSystem>().enabled = false;

        player.GetComponent<Player>().weaponEquipped = false;
        equipped = false;
        this.gameObject.SetActive(false);
    }
}