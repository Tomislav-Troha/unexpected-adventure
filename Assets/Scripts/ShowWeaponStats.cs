using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class ShowWeaponStats : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public bool hovered;

    public GameObject showStats;
    public Text txDamage;
    public Text txRange;
    public Text txMagazinSize;

    private GameObject pistol;
    private GameObject m4_carbine;

    public GameObject upgradePistolBtn;
    public GameObject upgradeM4CarbineBtn;

    void Start()
    {
        showStats.SetActive(false);
        hovered = false;

        pistol = GameObject.Find("Pistol");
        m4_carbine = GameObject.Find("M4_Carbine");
    }


    void Update()
    {
        if (pistol.GetComponent<UpgradeWeapon>().PistolcurrentLevel.text == "Level 4")
        {
            upgradePistolBtn.GetComponent<Button>().GetComponentInChildren<Text>().text = "Max level";
            upgradePistolBtn.GetComponent<Button>().interactable = false;
        }

        if(m4_carbine.GetComponent<UpgradeWeapon>().M4CarbinecurrentLevel.text == "Level 4")
        {
            upgradeM4CarbineBtn.GetComponent<Button>().GetComponentInChildren<Text>().text = "Max level";
            upgradeM4CarbineBtn.GetComponent<Button>().interactable = false;
        }


       
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        hovered = true;

        if (hovered == true && eventData.pointerCurrentRaycast.gameObject.name == "Pistol_img")
        {
           // print(pistol.GetComponent<Item>().type);
            showStats.SetActive(true);
            txDamage.text = "Damage: " + pistol.GetComponent<GunSystem>().damage.ToString();
            txMagazinSize.text = "Magazin size: " + pistol.GetComponent<GunSystem>().magazineSize.ToString();
            txRange.text = "Range: " + pistol.GetComponent<GunSystem>().range.ToString();
        }

        if(hovered == true && eventData.pointerCurrentRaycast.gameObject.name == "M4_Carbine_img")
        {
           // print(m4_carbine.GetComponent<Item>().type);
            showStats.SetActive(true);
            txDamage.text = "Damage: " + m4_carbine.GetComponent<GunSystem>().damage.ToString();
            txMagazinSize.text = "Magazin size: " + m4_carbine.GetComponent<GunSystem>().magazineSize.ToString();
            txRange.text = "Range: " + m4_carbine.GetComponent<GunSystem>().range.ToString();
        }




        if(hovered && eventData.pointerCurrentRaycast.gameObject.name == "Upgrade pistol txt" && pistol.GetComponent<UpgradeWeapon>().PistolcurrentLevel.text != "Level 4")
        {
            //print(eventData.pointerCurrentRaycast.gameObject.name);
            showStats.SetActive(true);
            txDamage.text = "Damage: " + pistol.GetComponent<GunSystem>().damage.ToString() + "<color=red>  +3</color>";
            txMagazinSize.text = "Magazin size: " + pistol.GetComponent<GunSystem>().magazineSize.ToString() + "<color=red>  +2</color>";
            txRange.text = "Range: " + pistol.GetComponent<GunSystem>().range.ToString() +  "<color=red>  +10</color>";

        }

        if (hovered && eventData.pointerCurrentRaycast.gameObject.name == "Upgrade m4Carbine txt" && m4_carbine.GetComponent<UpgradeWeapon>().M4CarbinecurrentLevel.text != "Level 4")
        {
            //print(eventData.pointerCurrentRaycast.gameObject.name);
            showStats.SetActive(true);
            txDamage.text = "Damage: " + m4_carbine.GetComponent<GunSystem>().damage.ToString() + "<color=red>  +3</color>";
            txMagazinSize.text = "Magazin size: " + m4_carbine.GetComponent<GunSystem>().magazineSize.ToString() + "<color=red>  +2</color>";
            txRange.text = "Range: " + m4_carbine.GetComponent<GunSystem>().range.ToString() + "<color=red>  +10</color>";
        }
    }

   
    public void OnPointerExit(PointerEventData eventData)
    {
        showStats.SetActive(false);
        hovered = false;
    }
}
