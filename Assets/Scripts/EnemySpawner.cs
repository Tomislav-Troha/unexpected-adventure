using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.PostProcessing;


public class EnemySpawner : MonoBehaviour
{
    [Header("Spawners & enemies")]
    public Transform[] spawners;
    public GameObject sickHuman;
    public GameObject sickHuman4Terrain;
    public GameObject firstBoss;
    public GameObject sickHumanSnow;
    public GameObject secondBoss;
    public GameObject sickHumanVillage;
    public GameObject thirdBoss;
    public GameObject Tirius;

    [Header("Audio")]
    public AudioSource source;
    public AudioClip scarySound;
    public AudioClip scaryTalk;
    public AudioClip hearthBeat;
    public AudioClip pianoJumpScare;
    public AudioClip freakAmbienceTirius;

    [Header("Game objects")]
    public GameObject trigger9;

    private GameObject subs;

    public static GameObject obj;

    private void Start()
    {
        subs = GameObject.FindWithTag("Subs");

    }

    public void spawn(Transform spawner, GameObject player)
    {


        if (spawner == spawners[0])
        {
           StartCoroutine(subs.GetComponent<Subtitles>().showSubsFirstEnemy());
           
 
           Instantiate(sickHuman, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            
           source.PlayOneShot(pianoJumpScare);
           
        }

        if (spawner == spawners[1])
        {
            ;
            for (int i = 0; i < 2; i++)
            {
                Instantiate(sickHuman, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            }
            //source.PlayOneShot(pianoJumpScare);

        }

        if (spawner == spawners[2])
        {
          

            for(int i = 0; i < 2; i++)
            {
                Instantiate(sickHuman, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            }
           
            source.PlayOneShot(pianoJumpScare);
        }

        //spawnanje prvog bossa
        if (spawner == spawners[3])
        {
            // print("Cetvrti");
            player.GetComponent<Player>().isReadyToKill = false;

            StartCoroutine(subs.GetComponent<Subtitles>().showSubsFirstBoss1());

            Instantiate(firstBoss, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);

            source.PlayOneShot(scarySound);
            source.PlayOneShot(scaryTalk);
            source.PlayOneShot(hearthBeat);


        }

        if (spawner == spawners[4])
        {
            for (int i = 0; i < 2; i++)
            {
                Instantiate(sickHumanSnow, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            }
            
            source.PlayOneShot(pianoJumpScare);
        }

        if (spawner == spawners[5])
        {
            for (int i = 0; i < 3; i++)
            {
                Instantiate(sickHumanSnow, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            }
           // source.PlayOneShot(pianoJumpScare);
        }

        if (spawner == spawners[6])
        {
            StartCoroutine(subs.GetComponent<Subtitles>().showSubsTrap());

            for (int i = 0; i < 2; i++)
            {
                Instantiate(sickHumanSnow, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            }

            source.PlayOneShot(pianoJumpScare);

            //destory collider na mostu u drugom dijelu, "maybe i need to check this house first"
            Destroy(trigger9);
        }

        //spawnanje drugog bossa
        if (spawner == spawners[7])
        {
            player.GetComponent<Player>().isReadyToKill = false;

            StartCoroutine(subs.GetComponent<Subtitles>().showSubsSecondBoss());

            Instantiate(secondBoss, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);

            source.PlayOneShot(pianoJumpScare);
            source.PlayOneShot(hearthBeat);
        }

        if (spawner == spawners[8])
        {
            Instantiate(sickHumanVillage, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            //source.PlayOneShot(pianoJumpScare);
        }

        if (spawner == spawners[9])
        {
            Instantiate(sickHumanVillage, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            //source.PlayOneShot(pianoJumpScare);
        }

        if (spawner == spawners[10])
        {
            Instantiate(sickHumanVillage, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            source.PlayOneShot(pianoJumpScare);
        }
        if (spawner == spawners[11])
        {
            Instantiate(sickHumanVillage, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            //source.PlayOneShot(pianoJumpScare);
        }
        if (spawner == spawners[12])
        {
            Instantiate(sickHumanVillage, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            //source.PlayOneShot(pianoJumpScare);
        }

        if (spawner == spawners[13])
        {
            Instantiate(sickHumanVillage, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            //source.PlayOneShot(pianoJumpScare);
        }

        if (spawner == spawners[14])
        {
            player.GetComponent<Player>().isReadyToKill = false;
            Instantiate(thirdBoss, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            source.PlayOneShot(hearthBeat); 
        }

        if (spawner == spawners[15])
        {
            for(int i = 0; i < 4; i++)
            {
                Instantiate(sickHuman4Terrain, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            }
            source.PlayOneShot(pianoJumpScare);
            StartCoroutine(subs.GetComponent<Subtitles>().showSubsOhNoAgainThoseGuys());
        }


        if (spawner == spawners[16])
        {
            for (int i = 0; i < 6; i++)
            {
                Instantiate(sickHuman4Terrain, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            }
            source.PlayOneShot(pianoJumpScare);
          
        }

        if (spawner == spawners[17])
        {
            player.GetComponent<Player>().isReadyToKill = false;
            Instantiate(Tirius, spawners[spawner.GetSiblingIndex()].position, spawners[spawner.GetSiblingIndex()].rotation);
            
            player.GetComponent<Player>().isReadyToKill = false;
            source.PlayOneShot(pianoJumpScare);

        }



    }

}

