using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;


public class Enemy : MonoBehaviour
{
    public float health;
    public GameObject[] item;
    public int typeOfEnemyID;


    private GameObject subs;

    public bool dead;

    public bool readyToKill;

    private CapsuleCollider capsuleCollider;
    private GameObject player;


    private int count = 0;

    private void Start()
    {
        capsuleCollider = GetComponent<CapsuleCollider>();
       
        player = GameObject.FindWithTag("Player");

        subs = GameObject.FindWithTag("Subs");
    }

    private void Update()
    {
        //print(pistol.GetComponent<GunSystem>().rayHit.collider.gameObject);
        //provjeri ako je health 0 dead postane true

       if(typeOfEnemyID == 999)
        {
            player.GetComponent<Player>().isReadyToKill = true;
        }


       if(health <= 0)
        {
            dead = true;
        }

    }

    public IEnumerator Die(GameObject gameObject)
    {

        //cekaj 5 sekundi da zavrsi animacija
        yield return new WaitForSeconds(3);

        for (int i = 0; i < item.Length; i++)
        {
            //provjeri dal ima u array-u neki drop
            if (item[i] == null)
            {
                print("Item in the enemy named " + this.gameObject.name + " has not been set! Missing ID: " + i);
            }
            count++;
        }
        //print("enemy umro " + gameObject);
        //ako gameObject(enemy) nije null dropaj item i makni enemy-a
        if (gameObject != null)
            {
            int Rand_num = UnityEngine.Random.Range(0, count);
            int all_num = count;

            if (typeOfEnemyID == 999)
                {
                print("count " + count);
                print("random num "+ Rand_num);
                Instantiate(item[Rand_num], transform.position, Quaternion.identity);
                Destroy(gameObject);
                }

             if(typeOfEnemyID == 1)
            {
                print("First boss count: " + all_num);
                Instantiate(item[0], transform.position, Quaternion.identity);
                Destroy(gameObject);
            }

            if (typeOfEnemyID == 2)
            {
                print("Second boss count: " + all_num);
                Instantiate(item[0], transform.position, Quaternion.identity);
                Destroy(gameObject);
            }

            if (typeOfEnemyID == 3)
            {
               
                StartCoroutine(subs.GetComponent<Subtitles>().showSubsThirdBossDie());
                yield return new WaitForSeconds(13);
                print("Third boss count: " + all_num);
                Instantiate(item[0], transform.position, Quaternion.identity);
                Destroy(gameObject);
            }

            if (typeOfEnemyID == 4)
            {
                StartCoroutine(subs.GetComponent<Subtitles>().showSubsTirius());
                yield return new WaitForSeconds(55);

                player.GetComponent<Player>().horrorAmbience.Stop();
                player.GetComponent<Player>().finalVideo.SetActive(true);
                player.GetComponent<Player>().finalVideo.GetComponent<VideoPlayer>().Play();
 
                yield return new WaitForSeconds(42);
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                SceneManager.LoadScene(0);
            }


        }
    }

}
