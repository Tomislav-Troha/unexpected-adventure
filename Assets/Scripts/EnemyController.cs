using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    private NavMeshAgent agent;
    private Animator anim = null;

    private Transform target;
    public float stoppingDistance;

    [Header("LayerMask")]
    public LayerMask whatIsGround;
    public LayerMask whatIsPlayer;

    [Header("Patroling")]
    //patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkingPointRange;

    [Header("Attacking")]
    //attacking
    public float timeBetweenAttack;
    public float attackDamage;
    bool alreadyAttack;

    [Header("States")]
    //states
    public float sightRange;
    public float attackRange;
    public bool playerInSightRange;
    public bool playerInAttackRange;

    private GameObject enemy;
    private GameObject player;


    private void Awake()
    {

    }


    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();

        player = GameObject.FindWithTag("Player");

        target = Player.instance;

        agent.updateRotation = true;
    }

    private void Update()
    {
        //provjeri da li je enemy u nasem vidu i vidu da nas mo�e napasti
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        //ako nije u vidu i vidu za napadanje, hodaj random
        if (!playerInSightRange && !playerInAttackRange)
        {
            anim.SetBool("enemy_walk", true);
            patroling();
        }

        //ako je vidu ali nije u vidu za napad, hadat ce prema nama
        if (playerInSightRange && !playerInAttackRange)
        {
            moveToPlayer();
            anim.SetBool("enemy_walk", true);
            anim.SetBool("enemy_idle", false);
            anim.SetBool("enemy_attack", false);
        }

        //ako je u oba vida napada nas
        if (playerInSightRange && playerInAttackRange)
        {
            anim.SetBool("enemy_walk", false);
            anim.SetBool("enemy_idle", true);
            anim.SetBool("enemy_attack", true);
            attackPlayer();
            
        }

        if (GetComponent<Enemy>().dead == true)
        {
            anim.SetBool("enemy_walk", false);
            anim.SetBool("enemy_idle", false);
            anim.SetBool("enemy_attack", false);
            anim.SetBool("enemy_dead", true);
            agent.SetDestination(new Vector3(0f, 0f, 0f)); 
            agent.isStopped = true;
        }

    }

    private void moveToPlayer()
    {
        agent.SetDestination(target.position);

        float distanceToTarget = Vector3.Distance(target.position, transform.position);
        //print("Distance: " + distanceToTarget + " Stopping distance " + agent.stoppingDistance);
        if (distanceToTarget <= agent.stoppingDistance)
        {
            //print("Rotate");    
            rotateToTarget();
        }
    }

    private void rotateToTarget()
    {
        //transform.LookAt(target);

        Vector3 dir = target.position - transform.position;
        dir.y = 0.0f;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir), 5f * Time.deltaTime);
    }

    public void patroling()
    {
        if (!walkPointSet) searchWalkPoint();

        if (walkPointSet)
        {
            agent.SetDestination(walkPoint);
        }

        Vector3 distanceToWalkPoint = transform.position - walkPoint;


        if (distanceToWalkPoint.magnitude < 1f)
        {
            walkPointSet = false;
        }
    }

    private void attackPlayer()
    {
        agent.SetDestination(transform.position);

       // transform.LookAt(target);


        if (!alreadyAttack)
        {
            //attack code here


            alreadyAttack = true;

            Invoke(nameof(resetAttack), timeBetweenAttack);
        }
    }

    private void resetAttack()
    {
        alreadyAttack = false;
    }

    //funckija koja daje random puteve za patroling enemy-a
    private void searchWalkPoint()
    {
        float randomZ = UnityEngine.Random.Range(-walkingPointRange, walkingPointRange);
        float randomX = UnityEngine.Random.Range(-walkingPointRange, walkingPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
        {
            walkPointSet = true;
        }
    }

}

