using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public GameObject shop;

    public int[,] shopItems = new int[4, 4];
    public float cristals;
    public Text txCristals;
    public Text txMetalScraps;

    public bool shopEnabled;
    private GameObject txPressToOpen;

    public GameObject traderPostiton;
    public GameObject traderPostiton1;
    public GameObject traderPostiton2;
    private GameObject trPos;

    private GameObject m4_carbine;
    private GameObject pistol;

    private GameObject playerMove;
    private GameObject cameraMove;

    [Header("References")]
    private float range = 5f;
    public Camera Camera_Environment;

    void Start()
    {
        txCristals.text = "Crystals: " + cristals;
        txMetalScraps.text = "Metal Scraps: " + cristals;

        txPressToOpen = GameObject.FindWithTag("Player");

        m4_carbine = GameObject.Find("M4_Carbine");
        pistol = GameObject.Find("Pistol");

        playerMove = GameObject.FindWithTag("Player");
        cameraMove = GameObject.FindWithTag("MainCamera");

        //id
        shopItems[1, 1] = 1;
        shopItems[1, 2] = 2;
        shopItems[1, 3] = 3;

        //novci
        shopItems[2, 1] = 10;
        shopItems[2, 2] = 20;
        shopItems[2, 3] = 30;

        //kolicina
        shopItems[3, 1] = 0;
        shopItems[3, 2] = 0;
        shopItems[3, 3] = 0;

    }

    private void Update()
    {
        // print(pistol.GetComponent<GunSystem>().reloading);
        txCristals.text = "Crystals " + cristals;
        txMetalScraps.text = "Metal Scraps: " + txPressToOpen.GetComponent<Inventory>().metalScraps;
        var ray = Camera_Environment.ViewportPointToRay(new Vector2(.5f, .5f));

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, range))
        {
            var selection = hit.collider.gameObject;
            trPos = selection;
            if (selection.CompareTag("Trader"))
            {
                txPressToOpen.GetComponent<Inventory>().pressToPickUp.text = "Press 'e' to trade";
                txPressToOpen.GetComponent<Inventory>().showText.SetActive(true);

                if (Input.GetKeyDown(KeyCode.E))
                {

                    shopEnabled = !shopEnabled;

                }

                if (shopEnabled)
                {
                    //ugasi pokrete i micanje
                    cameraMove.GetComponent<PlayerCam>().enabled = false;
                    playerMove.GetComponent<PlayerMovement>().enabled = false;

                    //otvori canvas od shop-a
                    shop.GetComponent<Canvas>().enabled = true;

                    //ugasi da mozes pucat
                    pistol.GetComponent<GunSystem>().reloading = true;
                    m4_carbine.GetComponent<GunSystem>().reloading = true;

                    //pokazi cursor
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = true;
                }

                if (shop.GetComponent<Canvas>().enabled == true)
                {
                    if (shopEnabled == false)
                    {
                        //upali pokrete i micanje
                        cameraMove.GetComponent<PlayerCam>().enabled = true;
                        playerMove.GetComponent<PlayerMovement>().enabled = true;

                        //zatvori canvas od shop-a
                        shop.GetComponent<Canvas>().enabled = false;

                        //upali da mozes pucat
                        pistol.GetComponent<GunSystem>().reloading = false;
                        m4_carbine.GetComponent<GunSystem>().reloading = false;

                        Cursor.lockState = CursorLockMode.Locked;
                        Cursor.visible = false;

                    }
                }


            }
        }

    }

    //funkcija koja se referira na kupovanje medKita i municije
    public void buy()
    {
        GameObject buttonRef = GameObject.FindGameObjectWithTag("Event").GetComponent<EventSystem>().currentSelectedGameObject;

        //print(buttonRef.gameObject.name);

        //klasican shop, ako imamo novaca dozvoli kupovanje i oduzmi novce, shopItem[2, ...] predstavlja da se radi o novcima jer smo tako definirali, da pise shopItem[3, ...] to je onda kolicina
        if (cristals >= shopItems[2, buttonRef.GetComponent<ShopButtonInfo>().itemID])
        {
            cristals -= shopItems[2, buttonRef.GetComponent<ShopButtonInfo>().itemID];

            //kada kupimo neki item kolicina nam se poveca za 1 da vidimo kolko smo kupili stvari
            shopItems[3, buttonRef.GetComponent<ShopButtonInfo>().itemID]++;

            txCristals.text = "Cristals " + cristals;

            buttonRef.GetComponent<ShopButtonInfo>().quantityTx.text = shopItems[3, buttonRef.GetComponent<ShopButtonInfo>().itemID].ToString();

            //dodajemo stvari u nas inventory ovisi sta kliknemo/kupimo
            if (buttonRef.gameObject.name == "ShopMedKit")
            {
                if(trPos.gameObject.name == "Trader")
                {
                    Instantiate(buttonRef.GetComponent<ShopButtonInfo>().firstAidObject, traderPostiton.transform.position, Quaternion.identity);
                }

                if (trPos.gameObject.name == "Trader1")
                {
                    Instantiate(buttonRef.GetComponent<ShopButtonInfo>().firstAidObject, traderPostiton1.transform.position, Quaternion.identity);
                }

                if (trPos.gameObject.name == "Trader2")
                {
                    Instantiate(buttonRef.GetComponent<ShopButtonInfo>().firstAidObject, traderPostiton2.transform.position, Quaternion.identity);
                }



            }

            if (buttonRef.gameObject.name == "ShopPistolAmmo")
            {
                pistol.GetComponent<GunSystem>().ammo += 10;
            }

            if (buttonRef.gameObject.name == "ShopM4Ammo")
            {
                m4_carbine.GetComponent<GunSystem>().ammo += 15;
            }
        }
    }
}