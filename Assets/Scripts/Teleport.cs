using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Teleport : MonoBehaviour
{
    public Transform teleportTarger;
    public GameObject thePlayer;
    public GameObject castleVideo;

    private IEnumerator OnTriggerEnter(Collider other)
    {
        
        if(other.gameObject.tag == "Player")
        {
            yield return new WaitForSeconds(3);
            castleVideo.SetActive(true);
            castleVideo.GetComponent<VideoPlayer>().Play();
            Destroy(castleVideo, 7f);
            other.gameObject.GetComponent<Player>().horrorAmbience.Stop();
            other.gameObject.GetComponent<Player>().source.PlayOneShot(other.gameObject.GetComponent<Player>().scaryTension);

            yield return new WaitForSeconds(7);
            thePlayer.transform.position = teleportTarger.transform.position;
            other.gameObject.GetComponent<Player>().horrorAmbience.Play();
            other.gameObject.GetComponent<Player>().source.Stop();


        }
        
    }
}
