using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class CutSceneController : MonoBehaviour
{
    public VideoPlayer videoPlayer;



    private void Update()
    {
        if (videoPlayer.isPaused) {
            SceneManager.LoadScene(2);
        }
    }
}
