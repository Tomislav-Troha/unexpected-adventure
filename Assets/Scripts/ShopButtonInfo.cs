using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopButtonInfo : MonoBehaviour
{
    public int itemID;
    public Text priceTx;
    public Text quantityTx;
    public GameObject shopManager;
    public GameObject firstAidObject;

    private void Update()
    {
        priceTx.text = "Price: " + shopManager.GetComponent<ShopManager>().shopItems[2, itemID].ToString();

        quantityTx.text = shopManager.GetComponent<ShopManager>().shopItems[3, itemID].ToString();
 

    }
}
