using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeleeSystem : MonoBehaviour
{
    [Header("Gun stas")]
    public int damage;
    public float timeBetweenShooting, range;

    //bools
    bool shooting;
    public bool readyToShoot;

    public static GameObject enemyObject;
    private Rigidbody rb;

    [Header("Reference")]
    public Camera fpsCam;
    public RaycastHit rayHit;
    public LayerMask whatIsEnemy;

    [Header("Audio")]
    public AudioSource source;
    public AudioClip axeSound;

    [Header("Graphics")]
    public GameObject HoleGraphic;

    private GameObject getPendulumXp;
    private GameObject getPlayer;


    private void Start()
    {
        this.enabled = false;

        getPendulumXp = GameObject.FindWithTag("Player");
        getPlayer = GameObject.FindWithTag("Player");
    }

    private void Awake()
    {
        readyToShoot = true;
    }

    private void Update()
    {
        myInput();
    }


    private void myInput()
    {
       
        shooting = Input.GetKeyDown(KeyCode.Mouse0);


        //pucanje
        if (readyToShoot && shooting)
        {
            source.PlayOneShot(axeSound);
            shoot();
        }
    }

    private void shoot()
    {
        readyToShoot = false;

        //izracunaj udaljenost sa spreadom
        Vector3 direction = fpsCam.transform.forward;
        if (Physics.Raycast(fpsCam.transform.position, direction, out rayHit, range, whatIsEnemy))
        {

            //Debug.Log(rayHit.collider.GetComponent<Enemy>().gameObject.name);

            //ako je rayHit enemy skidaj mu health sa definiranim damage-om
            if (rayHit.collider.CompareTag("Enemy"))
            {

                //ako smo mu dali potrbene iteme, sada ga mozemo ubiti
                if (getPlayer.GetComponent<Player>().isReadyToKill)
                {
                    rayHit.collider.GetComponent<Enemy>().health -= damage;
                    print("helath " + rayHit.collider.GetComponent<Enemy>().health);
                    //Debug.Log(rayHit.collider.GetComponent<Enemy>().health);

                    //dodaj xp na pendulum
                    getPendulumXp.GetComponent<Inventory>().pendulum.value += UnityEngine.Random.Range(3, 15);
                    //print(getPendulumXp.GetComponent<Inventory>().pendulum.value);
                }

                //ako enemy ima health manji il jednak 0 znaci da je mrtav
                if (rayHit.collider.GetComponent<Enemy>().health <= 0)
                {
                    rayHit.collider.GetComponent<Enemy>().GetComponent<CapsuleCollider>().enabled = false;
                    rayHit.collider.GetComponent<Enemy>().dead = true;
                }
           
                //ako je mrtav zaponci animaciju i funkciju Die iz skripte enemy
                if (rayHit.collider.GetComponent<Enemy>().dead == true)
                {
                    //zapocni korutinu Die funckije
                    print(rayHit.collider.gameObject);
                    StartCoroutine(rayHit.collider.GetComponent<Enemy>().Die(rayHit.collider.gameObject));
                }
            }
        }

        //muzzle flash i decali
        Instantiate(HoleGraphic, rayHit.point, Quaternion.Euler(0, 180, 0));

        Invoke("resetShoot", timeBetweenShooting);
    }

    private void resetShoot()
    {
        readyToShoot = true;
    }





}