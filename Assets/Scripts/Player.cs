using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class Player : MonoBehaviour
{

    #region
    public static Transform instance;

    private void Awake()
    {
        instance = this.transform;
    }
    #endregion


    public GameObject finalVideo;

    //variable
    public float maxHealth;
    public float curHealth;

    public Text interactText;

    public bool dead;

    public Text numText;
    public Slider healthBar;
   // public Rigidbody bullet;

    public bool weaponEquipped;

    public int axe_damage;
    public bool triggeringWithEnemy;
    private GameObject enemyObject;

    public bool isReadyToKill;
    public int enemyID;

    public bool yellowPill;
    public bool postIt;
    public bool teddyBear;
    public bool castle_img;
    public bool blue_pill;
    public bool white_pill;

    private GameObject subs;
    bool subsIsDone = false;
    bool thirdBoosSubsIsDone = false;

    [Header("Audio")]
    public AudioSource source;
    public AudioClip axeHit;
    public AudioClip scaryTension;

    public AudioSource horrorAmbience;

    // funkcije

    private void Start()
    {
        curHealth = maxHealth;
        healthBar.value = curHealth;
        healthBar.maxValue = maxHealth;
        numText.text = curHealth.ToString();

       // bullet = GetComponent<Rigidbody>();
        //numText = GetComponent<Text>();

        horrorAmbience.Play();

        subs = GameObject.FindWithTag("Subs");
    }

    private void Update()
    {

        enemyBehaviorByID();

    }

    public void Attack(GameObject target)
    {
        if (target.tag == "Enemy" && weaponEquipped)
        {
            Enemy enemy = target.GetComponent<Enemy>();
            enemy.health -= axe_damage;
        }
    }

    public void sendDamage(float damageValue)
    {

        curHealth -= damageValue;

        if (curHealth <= 0)
        {
            curHealth = 0;
            Die();
        }

        healthBar.value = curHealth;
        numText.text = curHealth.ToString();
    }


    public void Die()
    {
        dead = true;
        print("You are dead");
    }

    public void Heal(float increaseRate)
    {
        curHealth += increaseRate;

        if (curHealth >= maxHealth)
        {
            curHealth = maxHealth;
        }

        numText.text = curHealth.ToString();
        healthBar.value = curHealth;

    }

    /* private void OnTriggerEnter(Collider other)
     {
         if (other.tag == "Enemy")
         {
             enemyID = other.gameObject.GetComponent<Enemy>().typeOfEnemyID;
             if (enemyID == 999)
             {
                 isReadyToKill = true;
                 print("Enemy ID " + enemyID);

                 sendDamage(Random.Range(1, 3));
             }

             if (enemyID == 1)
             {
                 sendDamage(Random.Range(5, 10));
                 print("Enemy ID " + enemyID);
             }

             if (enemyID == 2)
             {
                 sendDamage(Random.Range(5, 15));
                 print("Enemy ID " + enemyID);
             }

         }
     }*/


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            enemyID = other.gameObject.GetComponent<Enemy>().typeOfEnemyID;
            if (enemyID == 999)
            {
                sendDamage(Random.Range(1, 3));

                enemyObject = other.gameObject;

                print("Enemy ID " + enemyID);

                isReadyToKill = true;
                triggeringWithEnemy = true;
            }

            if (enemyID == 1 || enemyID == 2 || enemyID == 3 || enemyID == 4)
            {
                // isReadyToKill = false;
                //print(other.gameObject);
                triggeringWithEnemy = true;
                enemyObject = other.gameObject;

                sendDamage(Random.Range(5, 15));

                print("Enemy ID " + enemyID);
            }

        }
    }

    private void OnTriggerExit(Collider other)
    {
        triggeringWithEnemy = false;
    }

    void enemyBehaviorByID()
    {
        //ako mu nismo dali potrebne item-e ne mo�emo ga ubit, ina?e ga mo�emo
        if (enemyID == 1)
        {
            if (yellowPill && postIt)
            {
                isReadyToKill = true;

                if (!subsIsDone)
                {
                    StartCoroutine(subs.GetComponent<Subtitles>().showSubsFirstBoss2());
                    subsIsDone = true;
                }
            }
        }

        if (enemyID == 2)
            {
                if (teddyBear)
                {
                    isReadyToKill = true;
                }
            }

        if(enemyID == 3)
        {
            if (castle_img && blue_pill)
            {
                isReadyToKill = true;

                if (!thirdBoosSubsIsDone)
                {
                    StartCoroutine(subs.GetComponent<Subtitles>().showSubsThirdBoss());
                    thirdBoosSubsIsDone = true;
                }
               
               
            }
        }

        if (enemyID == 4)
        {
            if (white_pill)
            {
                isReadyToKill = true;
            }
        }

    }
}
