using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnTrigger : MonoBehaviour
{


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && this.gameObject.tag == "spawner_one")
        {
            //transform.parent.gameObject.GetComponent<EnemySpawner>().spawnSickHumans_Wave1();
            gameObject.GetComponent<BoxCollider>().enabled = false;
           
            //print(this.gameObject.transform);
            transform.parent.gameObject.GetComponent<EnemySpawner>().spawn(this.gameObject.transform, other.gameObject);

        }


    }
}
