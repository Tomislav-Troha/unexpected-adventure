using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    public GameObject Canvas;
    private GameObject player;

    bool paused;

    private void Start()
    {
        Canvas.GetComponent<Canvas>().enabled = false;
        player = GameObject.FindWithTag("Player");

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
            

        }
       
        if (paused)
        {
            player.GetComponent<Inventory>().enabled = false;
            Canvas.GetComponent<Canvas>().enabled = true;

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            Time.timeScale = 0;

        }
        
        if(Canvas.GetComponent<Canvas>().enabled == true)
        {
            if (paused == false)
            {
                player.GetComponent<Inventory>().enabled = true;
                Canvas.GetComponent<Canvas>().enabled = false;
                Time.timeScale = 1;

                //pokazi cursor
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }
    }

    public void Resume()
    {

        paused = false;

        Time.timeScale = 1.0f;

        player.GetComponent<Inventory>().enabled = true;
        Canvas.GetComponent<Canvas>().enabled = false;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void ExitGame()
    {
        Application.Quit();
    }


}
