using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MenuController : MonoBehaviour
{
    [Header("Levels to load")]
    public string _playGame_lvl;
    private string levelToLoad;

    public AudioMixer audioMixer;
    public Slider volumeSlider;
    float currentVolume;

    int qualitiValue;


    private void Start()
    {
        volumeSlider.value = 1;
    }



    // kontrolira userov izbor na "Play game" popout-u
    public void PlayGameDialogYes(){
        SceneManager.LoadScene(1);
    }

// kontrolira izlaz iz igre
    public void ExitButton(){
        Application.Quit();
    }

	public void SetQualityLow()
	{
       // print("low setting applied");	
        QualitySettings.SetQualityLevel(1);
        qualitiValue = 1;

    }

    public void SetQualityMedium()
    {
       // print("medium setting applied");
        QualitySettings.SetQualityLevel(2);
        qualitiValue = 2;
    }

    public void SetQualityHigh()
    {
        //print("high setting applied");
        QualitySettings.SetQualityLevel(5);
        qualitiValue = 5;
    }

    public void SetVolume(float volume)
    {
        audioMixer.SetFloat("Volume", Mathf.Log10(volume)*20);
        currentVolume = volume;
    }

    public void SaveSettings()
    {
        PlayerPrefs.SetInt("QualitySettingPreference", qualitiValue);

                   
        PlayerPrefs.SetFloat("VolumePreference",currentVolume);
    }
}
