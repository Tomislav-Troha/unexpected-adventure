using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Subtitles : MonoBehaviour
{

    public Text subtitles;
    public Text chapters;
    public Transform[] triggers;

    [Header("Audio")]
    public AudioSource source;
    public AudioClip waterSound;

    public GameObject spawner;


    public IEnumerator showSubs(Transform trigger)
    {
        if (trigger == triggers[0])
        {
            subtitles.text = "Where am I?";
            yield return new WaitForSeconds(2);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "I need to get out of this place.";
            yield return new WaitForSeconds(2);
            subtitles.text = "";
        }

        if (trigger == triggers[1])
        {
            yield return new WaitForSeconds(1);
            subtitles.text = "Who is he..a trader?";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "Thank God, I need ammo.";
            yield return new WaitForSeconds(2);
            subtitles.text = "";
        }

        if(trigger == triggers[2])
        {
            yield return new WaitForSeconds(1);
            subtitles.text = "It's so beautiful up here.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
        }

        if (trigger == triggers[3])
        {
            subtitles.text = "I need something stronger to break this door.";
            yield return new WaitForSeconds(3);
            trigger.gameObject.GetComponent<BoxCollider>().enabled = false;
            subtitles.text = "";
        }

        if (trigger == triggers[4])
        {
            yield return new WaitForSeconds(1);
            subtitles.text = "Yes, I am pretty sure!";
            yield return new WaitForSeconds(2);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "It's always good to follow the signs.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(2);
            chapters.text = "Chapter 1: Post it, doze it";
            yield return new WaitForSeconds(3);
            chapters.text = "";
        }

        if (trigger == triggers[5])
        {
            yield return new WaitForSeconds(1);
            subtitles.text = "I just need to follow this path.";
            yield return new WaitForSeconds(2);
            subtitles.text = "";
        }

        if (trigger == triggers[6])
        {
            source.PlayOneShot(waterSound);
            yield return new WaitForSeconds(1);
            subtitles.text = "What is that, I need to hurry up.";
            yield return new WaitForSeconds(2);
            subtitles.text = "";
        }

        if (trigger == triggers[7])
        {
            yield return new WaitForSeconds(1);
            subtitles.text = "Maybe these mountains can give me a better view.";
            yield return new WaitForSeconds(2);
            subtitles.text = "";
        }

        if (trigger == triggers[8])
        {
            yield return new WaitForSeconds(1);
            subtitles.text = "Nice upgrade.";
            yield return new WaitForSeconds(2);
            subtitles.text = "";
            yield return new WaitForSeconds(2);
            chapters.text = "Chapter 2: Blind mind";
            yield return new WaitForSeconds(3);
            chapters.text = "";
        }

        if (trigger == triggers[9])
        {
            subtitles.text = "Maybe I need to check this house first";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
        }

        if (trigger == triggers[10])
        {
            subtitles.text = "<color=yellow>TIRIUS: </color>Once you slay one fear, you�ll conquer many fears.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "Who was that!?";
            yield return new WaitForSeconds(2);
            subtitles.text = "";
        }

        if (trigger == triggers[11])
        {
            subtitles.text = "Again this wierd guy..I need to upgrade my gun.";
            yield return new WaitForSeconds(2);
            subtitles.text = "";
        }

        if (trigger == triggers[12])
        {
            yield return new WaitForSeconds(1);
            subtitles.text = "What just happend?";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "That was strange.";
            yield return new WaitForSeconds(2);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "I should get to the village.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(2);
            chapters.text = "Chapter 3: I see a gun, I don�t run";
            yield return new WaitForSeconds(3);
            chapters.text = "";
        }

        if (trigger == triggers[13])
        {
            yield return new WaitForSeconds(1);
            subtitles.text = "After all this mileage I've finally reached village";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
        }

        if (trigger == triggers[14])
        {
            yield return new WaitForSeconds(1);
            subtitles.text = "Oh no, another one! What's your problem?";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "<color=yellow>BOSS: </color>What's your problem?";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
        }

        if (trigger == triggers[15])
        {
            subtitles.text = "CAS..hmm? CASTLE!";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "145 shouldn't be something far away.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "I'll just keep walking straight.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "Oh wait, S could be straight..";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "LLR is then LEFT and RIGHT.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "I guess I'm RIGHT about this.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
        }

        if (trigger == triggers[16])
        {
            subtitles.text = "Ok, now left";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
        }

        if (trigger == triggers[17])
        {
            subtitles.text = "SLLR..it's left again.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
        }

        if (trigger == triggers[18])
        {
            subtitles.text = "Wow, this castle is big, who lives in there!?";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "Let's check it out.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            chapters.text = "Tirius";
            yield return new WaitForSeconds(3);
            chapters.text = "";
        }

        if (trigger == triggers[19])
        {
            subtitles.text = "<color=yellow>TIRIUS: </color>Ever since you were a kid.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "<color=yellow>TIRIUS: </color>Play and not sitting still is all you did.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "<color=yellow>TIRIUS: </color>You could color and draw, jump like a daw.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "<color=yellow>TIRIUS: </color>Break your jaw, but the yellow pill was always the last straw.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "Why can I hear him?";
            yield return new WaitForSeconds(4);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "So if yellow means happy, why was blue nappy ?";
            yield return new WaitForSeconds(4);
            subtitles.text = "";
            
        }

        if (trigger == triggers[20])
        {
            spawner.GetComponent<BoxCollider>().enabled = true;

            subtitles.text = "<color=yellow>TIRIUS: </color>You got older, and your illness folder, just got bolder.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "<color=yellow>TIRIUS: </color>Your parents bought you a phone, but you couldn�t be left alone.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "<color=yellow>TIRIUS: </color>Getting a drone, making a clone.";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "<color=yellow>TIRIUS: </color>They were really prone, to put you in a zone!";
            yield return new WaitForSeconds(3);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "Cut the poetry and rhyme stuff! WHAT IS 'other than that means no chat'?!";
            yield return new WaitForSeconds(4);
            subtitles.text = "";
            yield return new WaitForSeconds(1);
            subtitles.text = "Ohh yes, I shouldn't talk, I have to give him the remaining pill";
            yield return new WaitForSeconds(4);
            subtitles.text = "";
        }

    }

    public IEnumerator showSubsTirius()
    {
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>TIRIUS: </color>I can�t adore, I�ll use some more";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>TIRIUS: </color>Rhymes really help me get to the core.";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(2);
        subtitles.text = "<color=yellow>TIRIUS: </color>You were a soldier even before you had gone to war.";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>TIRIUS: </color>Inner fights and battles you�ve lost were enormous and left you crippling.";
        yield return new WaitForSeconds(4);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>TIRIUS: </color>Somehow, you weren�t afraid of being tamed.";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>TIRIUS: </color>It came to me that you�ve found ZEN.";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>TIRIUS: </color>You were a soldier even before you had gone to war.";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>TIRIUS: </color>As time passed by I realized I was wrong; zen doesn�t include bloodthirsty flashbacks and severe nightmares.";
        yield return new WaitForSeconds(5);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "This explains the 'I see a gun, I don�t run!' and my playing God attitude.";
        yield return new WaitForSeconds(4);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>TIRIUS: </color>The same attitude that led you to the final battle.";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>TIRIUS: </color>Even though you looked like a broken children�s rattle.";
        yield return new WaitForSeconds(4);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>TIRIUS: </color>Life took the best from you and left nothing for the rest.";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>TIRIUS: </color>BUT, the last words I shall say, you are on the right way.";
        yield return new WaitForSeconds(5);
        subtitles.text = "";
    }


    public IEnumerator showSubsOhNoAgainThoseGuys()
    {
        yield return new WaitForSeconds(1);
        subtitles.text = "Ohh no, again those guys!!";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
    }


    public IEnumerator showSubsFirstEnemy()
    {
        yield return new WaitForSeconds(1);
        subtitles.text = "What is that?";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "Let's kill it.";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
    }

    public IEnumerator showSubsFirstBoss1()
    {
        subtitles.text = "This one is different.";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "Why is he so hyperactive!?";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "Oh... I know why.";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "He has ADHD.";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "I need to distract him.";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "So, Yellow means nappy...";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "OK...and post-it.";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "Let's try it.";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
    }

    public IEnumerator showSubsFirstBoss2()
    {
        yield return new WaitForSeconds(1);
        subtitles.text = "Oh yes, it's working...now you die!";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
    }

    public IEnumerator showSubsTrap()
    {
        yield return new WaitForSeconds(1);
        subtitles.text = "It's a trap!!!";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
    }


    public IEnumerator showSubsSecondBoss()
    {
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>BOSS: </color>Your mind is blind, but don't hassle about the castle";
        yield return new WaitForSeconds(4);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "What?";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "<color=yellow>BOSS: </color>If you want to pass, you need to give me something to relax.";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "Ohh, He wants my teddy bear, he's got BPD.";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "What's the matter, you want your toy back?";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "Let's give it to him.";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
    }

    public IEnumerator showSubsThirdBoss()
    {
        subtitles.text = "This!";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
    }

    public IEnumerator showSubsThirdBossDie()
    {
        subtitles.text = "<color=yellow>BOSS: </color>Tirius is mysterious, but my gun knows where�s the fun.";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "What does this mean? Is this a new hint? Hmmm�.";
        yield return new WaitForSeconds(3);
        subtitles.text = "";
        yield return new WaitForSeconds(1);
        subtitles.text = "145 SLLR?";
        yield return new WaitForSeconds(2);
        subtitles.text = "";
    }


}
