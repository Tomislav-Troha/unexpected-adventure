using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement")]
    public float moveSpeed;
    public float sprintSpeed;

    public float groundDrag;

    public float jumpForce;
    public float jumpCooldown;
    public float airMultiplier;
    bool readyToJump = true;

    [Header("Keybinds")]
    public KeyCode jumpKey = KeyCode.Space;
    public KeyCode sprintKey = KeyCode.LeftShift;

    [Header("Ground Check")]
    public float playerHeight;
    public LayerMask whatIsGround;
    bool grounded;

    [Header("Footsteps")]
    public AudioSource source;
    public AudioClip[] footsteps;
    public AudioClip sprintFootsteps;

    public Transform orientation;

    float horizonalInput;
    float verticalInput;

    Vector3 moveDiretion;
    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
    }

    private void Update()
    {

        if(grounded && rb.velocity.magnitude > 2f && rb.velocity.magnitude < 7.5f && source.isPlaying == false)
        {
            source.clip = footsteps[Random.Range(0, footsteps.Length)];
            source.volume = 0.1f;
            source.Play();
        }
        else if(grounded && rb.velocity.magnitude > 7.5f && source.isPlaying == false)
        {
            source.volume = 0.1f;
            source.clip = sprintFootsteps;
            source.Play();
        }
        else if(rb.velocity.magnitude < 2f)
        {
            source.volume = 0.5f;
        }

       // print(rb.velocity.magnitude);

        //provjeri ako smo na podu
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.2f, whatIsGround);

        myInput();
        speedControl();

        //ako smo prizemljeni (true)
        if (grounded)
        {
            
            //sprijeci klizanje
            rb.drag = groundDrag;
            //readyToJump = true;

            //ako kliknemo tipku za sprint(left shift) idemo brze
            if (Input.GetKey(sprintKey) && grounded)
            {
                moveSpeed = sprintSpeed;
            }
            else moveSpeed = 7;

        }
        else
        {
            rb.drag = 0;
        }

    }

    private void FixedUpdate()
    {
        movePlayer();
    }

    private void myInput()
    {
        horizonalInput = Input.GetAxisRaw("Horizontal");
        verticalInput = Input.GetAxisRaw("Vertical");


        //print(readyToJump);
        if (Input.GetKeyDown(jumpKey) && readyToJump && grounded)
          {
              readyToJump = false;

              Jump();

              Invoke(nameof(resetJump), jumpCooldown);
          }
    }

    //funckcija za kretanje sa tipkovnicom
    private void movePlayer()
    {
        moveDiretion = orientation.forward * verticalInput + orientation.right * horizonalInput;

        if (grounded)
        {
            rb.AddForce(moveDiretion.normalized * moveSpeed * 10f, ForceMode.Force);
        }
        else if (!grounded)
        {
            rb.AddForce(moveDiretion.normalized * moveSpeed * 10f * airMultiplier, ForceMode.Force);
        }

    }

    private void speedControl()
    {
        Vector3 flatVel = new Vector3(rb.velocity.x, 0f, rb.velocity.z);

        if (flatVel.magnitude > moveSpeed)
        {
            Vector3 limitedVel = flatVel.normalized * moveSpeed;
            rb.velocity = new Vector3(limitedVel.x, rb.velocity.y, limitedVel.z);
        }
    }

     private void Jump()
     {
         rb.velocity = new Vector3(rb.velocity.x, 0f, rb.velocity.z);


         rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
     }

     private void resetJump()
     {
        readyToJump = true;
     }
}