using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    private GameObject m4_carbine;
    private GameObject pistol;
    private GameObject axe;

    private bool hovered;
    public bool empty;

    public GameObject item;
    public Texture itemIcon;

    private GameObject player;
    private GameObject enemyController;

    public AudioSource M4Equipped;
    public AudioSource PistolEquipped;
    public AudioSource HealthEquipped;

    public Text interactText;

    public GameObject armsPistol;
    public GameObject armsNoGun;
    public GameObject armsM4;

    private void Start()
    {

        hovered = false;

        player = GameObject.FindWithTag("Player");

        m4_carbine = GameObject.Find("M4_Carbine");
        pistol = GameObject.Find("Pistol");
        axe = GameObject.Find("Axe");

        enemyController = GameObject.FindWithTag("Enemy");

        // M4Equipped = GetComponent<AudioSource>();
        // PistolEquipped = GetComponent<AudioSource>();

    }

    private void Update()
    {
        // ako je slot prazan stavi sliku u inventory, ako nije null
        if (item)
        {
            empty = false;

            itemIcon = item.GetComponent<Item>().icon;
            this.GetComponent<RawImage>().texture = itemIcon;

        }
        else
        {
            empty = true;
            itemIcon = null;
            this.GetComponent<RawImage>().texture = null;
        }

        //polozaj ruke u odnosu na oruzije
        if (player.GetComponent<Player>().weaponEquipped == false)
        {
            armsPistol.SetActive(false);
            armsM4.SetActive(false);
            armsNoGun.SetActive(true);
        }
    }

    // kada hoveramo u inventryu pokazuju nam se podaci o itemu
    public void OnPointerEnter(PointerEventData eventData)
    {
        hovered = true;

        if (item && hovered == true)
        {
            Item thisItem = item.GetComponent<Item>();

            //pogledaj za tip itema
            if (thisItem.type == "MedKit")
            {
                interactText.enabled = true;
                interactText.text = thisItem.type + " (+20 health)";
                //print(thisItem.type);

            }

            /*    if (thisItem.type == "pistolAmmo")
                {
                    interactText.enabled = true;
                    interactText.text = thisItem.name.ToString() + " (+10 bullets)";
                    //print(thisItem.type);

                }

                if (thisItem.type == "M4_ammo")
                {
                    interactText.enabled = true;
                    interactText.text = thisItem.name.ToString() + " (+15 bullets)";
                    //print(thisItem.type);

                } */

            if (thisItem.type == "axe")
            {
                interactText.enabled = true;
                interactText.text = thisItem.type;
                //print(thisItem.type);

            }

            if (thisItem.type == "pistol")
            {
                interactText.enabled = true;
                interactText.text = thisItem.type + " (145  SLLR)";
                //print(thisItem.type);

            }

            if (thisItem.type == "m4_carbine")
            {
                interactText.enabled = true;
                interactText.text = thisItem.type;
                //print(thisItem.type);

            }

            if (thisItem.type == "yellow_pill")
            {
                interactText.enabled = true;
                interactText.text = thisItem.type;
                //print(thisItem.type);

            }

            if (thisItem.type == "white_pill")
            {
                interactText.enabled = true;
                interactText.text = thisItem.type;
                //print(thisItem.type);

            }

            if (thisItem.type == "blue_pill")
            {
                interactText.enabled = true;
                interactText.text = thisItem.type;
                //print(thisItem.type);

            }

            if (thisItem.type == "postIt")
            {
                interactText.enabled = true;
                interactText.text = thisItem.type;
                //print(thisItem.type);

            }

            if (thisItem.type == "teddy_bear")
            {
                interactText.enabled = true;
                interactText.text = thisItem.type + " (BDP)";
                //print(thisItem.type);

            }

            if (thisItem.type == "castle_img")
            {
                interactText.enabled = true;
                interactText.text = "Image of castle";
                //print(thisItem.type);
            }
            
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hovered = false;
        interactText.enabled = false;
    }

    //kada kliknemo na item triggera se posebna funckija, npr za equipp, medkit...
    public void OnPointerClick(PointerEventData eventData)
    {
        if (item)
        {
            Item thisItem = item.GetComponent<Item>();

            //pogledaj za tip itema
            if (thisItem.type == "MedKit")
            {
                HealthEquipped.Play();
                // print(thisItem.type);
                player.GetComponent<Player>().Heal(thisItem.increaseRate);
                Destroy(item);
            }

            if (thisItem.type == "firstNote")
            {
                player.GetComponent<Inventory>().inventoryEnabled = false;
                player.GetComponent<Inventory>().showNote.SetActive(true);
            }

            if (thisItem.type == "yellow_pill")
            {
                if (player.GetComponent<Player>().triggeringWithEnemy && player.GetComponent<Player>().enemyID == 1)
                {
                    player.GetComponent<Player>().yellowPill = true;
                    Destroy(item);
                }
            }

            if (thisItem.type == "postIt")
            {
                if (player.GetComponent<Player>().triggeringWithEnemy && player.GetComponent<Player>().enemyID == 1)
                {
                    player.GetComponent<Player>().postIt = true;
                    Destroy(item);
                }
            }

            if (thisItem.type == "teddy_bear")
            {
                if (player.GetComponent<Player>().triggeringWithEnemy && player.GetComponent<Player>().enemyID == 2)
                {
                    player.GetComponent<Player>().teddyBear = true;
                    Destroy(item);
                }
            }

            if (thisItem.type == "castle_img")
            {
                if (player.GetComponent<Player>().triggeringWithEnemy && player.GetComponent<Player>().enemyID == 3)
                {
                    player.GetComponent<Player>().castle_img = true;
                    player.GetComponent<Player>().blue_pill = true;
                    Destroy(item);
                }
            }

            if (thisItem.type == "white_pill")
            {
                if (player.GetComponent<Player>().triggeringWithEnemy && player.GetComponent<Player>().enemyID == 4)
                {
                    player.GetComponent<Player>().white_pill = true;
                    Destroy(item);
                }
            }

            /* if (thisItem.type == "pistolAmmo")
              {
                 interactText.enabled = true;
                 interactText.text = thisItem.name.ToString() + " (+10 bullets)";
                 pistol.GetComponent<GunSystem>().ammo += 10;
                 Destroy(item);
                 //print(thisItem.type);

             }

              if (thisItem.type == "M4_ammo")
            {
                interactText.enabled = true;
                interactText.text = thisItem.name.ToString() + " (+15 bullets)";
                m4_carbine.GetComponent<GunSystem>().ammo += 15;
                Destroy(item);
                 //print(thisItem.type);

             } */

            if (thisItem.type == "pistol" && player.GetComponent<Player>().weaponEquipped == false)
            {
                player.GetComponent<Inventory>().inventoryEnabled = false;
                PistolEquipped.Play();

                thisItem.equipped = true;
                item.SetActive(true);

                pistol.GetComponent<GunSystem>().textHolder.enabled = true;
                pistol.GetComponent<GunSystem>().text.enabled = true;
                pistol.GetComponent<GunSystem>().enabled = true;
                player.GetComponent<Player>().weaponEquipped = true;

                armsPistol.SetActive(true);
                armsM4.SetActive(false);
                armsNoGun.SetActive(false);
            }

            if (thisItem.type == "m4_carbine" && player.GetComponent<Player>().weaponEquipped == false)
            {
                player.GetComponent<Inventory>().inventoryEnabled = false;
                M4Equipped.Play();

                thisItem.equipped = true;
                item.SetActive(true);

                m4_carbine.GetComponent<GunSystem>().enabled = true;
                m4_carbine.GetComponent<GunSystem>().textHolder.enabled = true;
                m4_carbine.GetComponent<GunSystem>().text.enabled = true;
                player.GetComponent<Player>().weaponEquipped = true;

                //print(thisItem.type);
                //print(m4_carbine.GetComponent<Item>().equipped);

                armsM4.SetActive(true);
                armsPistol.SetActive(false);
                armsNoGun.SetActive(false);
            }

            if (thisItem.type == "axe" && player.GetComponent<Player>().weaponEquipped == false)
            {
                axe.GetComponent<MeleeSystem>().enabled = true;
                player.GetComponent<Inventory>().inventoryEnabled = false;
                thisItem.equipped = true;
                item.SetActive(true);

                player.GetComponent<Player>().weaponEquipped = true;
            }
        }
    }

}