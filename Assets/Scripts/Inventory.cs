using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    //cijeli canvas, bit ce disable/enable
    public GameObject inventory;
    public GameObject slotHolder;
    public GameObject itemManager;

    public GameObject shopManager;
    public Text cristals;

    public int metalScraps;
    public Text txMetalScraps;

    //koliko slotova imamo
    private int slots;

    //dohvacanje svakog slota posebno
    private Transform[] slot;

    private GameObject itemPickedUp;

    public Text pressToPickUp;
    public GameObject showText;

    public GameObject showNote;

    public bool inventoryEnabled;
    private bool addedItem;
    private bool noteTextEnabled;

    public string selectableTag = "PickUps";
    private float range = 5f;
    public Camera Camera_Environment;
    public GameObject UI_Interactable;

    //pendulum
    public Slider pendulum;

    private Transform _selection;

    private GameObject axe;
    private GameObject m4_carbine;
    private GameObject pistol;

    private GameObject playerMove;
    private GameObject cameraMove;

    [Header("Sound")]
    public AudioSource audioSource;
    public AudioClip crystalPickUp;
    public AudioClip ammoPickUp;
    public AudioClip metalScrapPickUp;


    private void Start()
    {
        pendulum.gameObject.SetActive(false);

        // gunSystem = GameObject.FindWithTag("PickUps");
        m4_carbine = GameObject.Find("M4_Carbine");
        pistol = GameObject.Find("Pistol");
        axe = GameObject.Find("Axe");

        playerMove = GameObject.FindWithTag("Player");
        cameraMove = GameObject.FindWithTag("MainCamera");

        showText.SetActive(false);

        //slotovi su detektirani
        slots = slotHolder.transform.childCount;
        slot = new Transform[slots];
        detectInventorySlots();
    }
    private void Update()
    {
        //provjeri ako je pendulum 100 povecaj nam snagu i stavi pendulum na 0
        if (pendulum.value == 100)
        {
            playerMove.GetComponent<Player>().maxHealth += 10;
            playerMove.GetComponent<Player>().healthBar.maxValue = playerMove.GetComponent<Player>().maxHealth;

            pendulum.value = 0;
        }

        cristals.text = "Crystals " + shopManager.GetComponent<ShopManager>().cristals;
        txMetalScraps.text = "Metal scraps " + metalScraps;

        if (Input.GetKeyDown(KeyCode.I))
        {
            inventoryEnabled = !inventoryEnabled;

        }

        if (inventoryEnabled)
        {
            //ugasi pokrete i micanje
            cameraMove.GetComponent<PlayerCam>().enabled = false;
            playerMove.GetComponent<PlayerMovement>().enabled = false;

            //otvori inventroy
            inventory.GetComponent<Canvas>().enabled = true;

            //ugasi da mozes pucat
            pistol.GetComponent<GunSystem>().reloading = true;
            m4_carbine.GetComponent<GunSystem>().reloading = true;
            axe.GetComponent<MeleeSystem>().readyToShoot = false;

            //pokazi cursor
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (inventory.GetComponent<Canvas>().enabled == true)
        {

            if (inventoryEnabled == false)
            {
                //upali pokrete i micanje
                cameraMove.GetComponent<PlayerCam>().enabled = true;
                playerMove.GetComponent<PlayerMovement>().enabled = true;

                //zatvori inventroy
                inventory.GetComponent<Canvas>().enabled = false;

                //upali da mozes pucat
                pistol.GetComponent<GunSystem>().reloading = false;
                m4_carbine.GetComponent<GunSystem>().reloading = false;
                axe.GetComponent<MeleeSystem>().readyToShoot = true;


                //pokazi cursor
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

        }

        // provjeri je li note otvoren, ako je stisni e za zatvoriti
        if (showNote)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {

                showNote.SetActive(false);
            }
        }

        //ovo tocno ne znam sta je ali bojim se obrisat :/
        if (_selection != null)
        {
            UI_Interactable.SetActive(false);
            _selection = null;
        }

        //RayCast da provjerimo kad smo na nekom od itema
        var ray = Camera_Environment.ViewportPointToRay(new Vector3(.5f, .5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, range))
        {
            var selection = hit.collider.gameObject;
            //kada nam je cursor na odabranom tagu pokazi tekst da ga mozemo pokupiti
            if (selection.CompareTag(selectableTag))
            {
                showText.SetActive(true);
                // print(hit.collider.gameObject.ToString());
                pressToPickUp.text = "Press 'e' to pick up " + hit.collider.gameObject.GetComponent<Item>().type;

                //kada stisnemo slovo e doda nam se item u inventory ili ako se radi o ammo/crystalima onda se direktno pribrojavaju
                if (Input.GetKeyDown(KeyCode.E))
                {
                    // Debug.Log("Player pressed E");
                    //itemPickedUp = other.gameObject;
                    itemPickedUp = hit.collider.gameObject;

                    // ako se radi o municiiji direktno pribrojava
                    if (itemPickedUp.GetComponent<Item>().type == "pistolAmmo")
                    {
                        audioSource.PlayOneShot(ammoPickUp);
                        pistol.GetComponent<GunSystem>().ammo += 5;
                        Destroy(itemPickedUp);
                    }
                    else if (itemPickedUp.GetComponent<Item>().type == "M4_ammo")
                    {
                        audioSource.PlayOneShot(ammoPickUp);
                        m4_carbine.GetComponent<GunSystem>().ammo += 15;
                        Destroy(itemPickedUp);
                    }
                    // ako se radi o kristalima direktno pribrojava
                    else if (itemPickedUp.GetComponent<Item>().type == "crystals")
                    {
                        audioSource.PlayOneShot(crystalPickUp);
                        shopManager.GetComponent<ShopManager>().cristals += Random.Range(10, 20);
                        Destroy(itemPickedUp);
                    }

                    // ako se radi o metal srapovima direktno pribrojava
                    else if (itemPickedUp.GetComponent<Item>().type == "metal_scraps")
                    {
                        audioSource.PlayOneShot(metalScrapPickUp);
                        metalScraps += Random.Range(10, 20);
                        Destroy(itemPickedUp);
                    }

                    //kad pokupumo pendulum pokaze nam se xp bar
                    else if (itemPickedUp.GetComponent<Item>().type == "pendulum")
                    {
                        pendulum.gameObject.SetActive(true);
                       // pendulum.value = 0;
                        Destroy(itemPickedUp);
                    }
                    else
                    {
                        addedItem = false;
                        addItem(itemPickedUp);
                    }

                }

                // kada dodemo kod tradera samo nam se pokaze da se mozemo tradeat sa njim
                else if (selection.CompareTag("Trader"))
                {
                    showText.SetActive(true);
                    pressToPickUp.text = "Press 'e' to trade with merhcant";
                }

            }
            else showText.SetActive(false);

        }
    }

    //funkcija koja dodaje item u inventory
    public void addItem(GameObject item)
    {
        for (int i = 0; i < slots; i++)
        {
            if (slot[i].GetComponent<Slot>().empty && addedItem == false)
            {

                //stavlja nam ikonu i item koji smo pokupili
                slot[i].GetComponent<Slot>().item = item;
                slot[i].GetComponent<Slot>().itemIcon = item.GetComponent<Item>().icon;

                //ako se radi o puski taj item ode u itemManager
                item.transform.position = itemManager.transform.position;
                item.transform.parent = itemManager.transform;

                item.transform.localPosition = item.GetComponent<Item>().position;
                item.transform.localEulerAngles = item.GetComponent<Item>().rotation;
                item.transform.localScale = item.GetComponent<Item>().scale;

                item.SetActive(false);

                item.GetComponent<Item>().pickedUp = true;

                Destroy(item.GetComponent<Rigidbody>());
                addedItem = true;

            }
        }
    }

    //funkcija koja detektira slot gdje ce biti sljedeci element/item
    public void detectInventorySlots()
    {

        for (int i = 0; i < slots; i++)
        {
            slot[i] = slotHolder.transform.GetChild(i);

        }

    }
}