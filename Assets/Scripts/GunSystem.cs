using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunSystem : MonoBehaviour
{
    [Header("Gun stas")]
    public int damage;
    public float timeBetweenShooting, spread, range, reloadTime, timeBetweenShots;
    public int ammo;
    public int magazineSize, bulletsPerTap;
    public bool allowButtonHold;
    int bulletsLeft, bulletsShot;

    //bools
    bool readyToShoot, shooting;
    public bool reloading;

    public static GameObject enemyObject;
    private Rigidbody rb;

    [Header("Reference")]
    public Camera fpsCam;
    public Transform attackPoint;
    public RaycastHit rayHit;
    public LayerMask whatIsEnemy;

    [Header("Graphics")]
    public GameObject muzzleFlash, bulletHoleGraphic;
    public Text text;
    public Image textHolder;

    [Header("Audio")]
    public AudioSource reloadSounds;
    public AudioSource gunShootSound;

    private GameObject getPendulumXp;
    private GameObject getPlayer;
    // public AudioSource pistolReload;
    //  public AudioSource M4Reload;

    private void Start()
    {
        this.enabled = false;
        textHolder.enabled = false;

        getPendulumXp = GameObject.FindWithTag("Player");
        getPlayer = GameObject.FindWithTag("Player");
    }

    private void Awake()
    {
        bulletsLeft = magazineSize;
        readyToShoot = true;
    }

    private void Update()
    {
        myInput();

        //set text
        text.text = bulletsLeft + " / " + ammo;
    }


    private void myInput()
    {
        // ako je puska automatksa dopusti drzanje lijevog klika za pucanje
        if (allowButtonHold) shooting = Input.GetKey(KeyCode.Mouse0);
        else shooting = Input.GetKeyDown(KeyCode.Mouse0);

        //provjeri spremnost za reload
        if (Input.GetKeyDown(KeyCode.R) && bulletsLeft < magazineSize && !reloading && ammo != 0)
        {
            reload();
        }

        //pucanje
        if (readyToShoot && shooting && !reloading && bulletsLeft > 0)
        {
            gunShootSound.Play();
            bulletsShot = bulletsPerTap;
            shoot();
        }
    }

    private void shoot()
    {
        readyToShoot = false;

        float x = UnityEngine.Random.Range(-spread, spread);
        float y = UnityEngine.Random.Range(-spread, spread);

        //spread za rigidbody
        /*  if (rb.velocity.magnitude > 0)
          {
              spread = spread * 1.5f;
          }
          else spread = 0;*/

        //izracunaj udaljenost sa spreadom
        Vector3 direction = fpsCam.transform.forward + new Vector3(x, y, 0);
        if (Physics.Raycast(fpsCam.transform.position, direction, out rayHit, range, whatIsEnemy))
        {

            //Debug.Log(rayHit.collider.GetComponent<Enemy>().gameObject.name);

            //ako je rayHit enemy skidaj mu health sa definiranim damage-om
            if (rayHit.collider.CompareTag("Enemy"))
            {

                //ako smo mu dali potrbene iteme, sada ga mozemo ubiti
                if (getPlayer.GetComponent<Player>().isReadyToKill)
                {
                    rayHit.collider.GetComponent<Enemy>().health -= damage;
                    print("helath " + rayHit.collider.GetComponent<Enemy>().health);
                    //Debug.Log(rayHit.collider.GetComponent<Enemy>().health);

                    //dodaj xp na pendulum
                    getPendulumXp.GetComponent<Inventory>().pendulum.value += UnityEngine.Random.Range(3, 15);
                    //print(getPendulumXp.GetComponent<Inventory>().pendulum.value);
                }

                //ako enemy ima health manji il jednak 0 znaci da je mrtav
                if (rayHit.collider.GetComponent<Enemy>().health <= 0)
                {
                    rayHit.collider.GetComponent<Enemy>().GetComponent<CapsuleCollider>().enabled = false;
                    rayHit.collider.GetComponent<Enemy>().dead = true;
                }

                //ako je mrtav zaponci animaciju i funkciju Die iz skripte enemy
                if (rayHit.collider.GetComponent<Enemy>().dead == true)
                {
                    //zapocni korutinu Die funckije
                    print(rayHit.collider.gameObject);
                    StartCoroutine(rayHit.collider.GetComponent<Enemy>().Die(rayHit.collider.gameObject));
                }
            }
        }

        //muzzle flash i decali
        Instantiate(bulletHoleGraphic, rayHit.point, Quaternion.Euler(0, 180, 0));
        
       Instantiate(muzzleFlash, attackPoint.position, Quaternion.identity);

        bulletsLeft--;
        bulletsShot--;

        Invoke("resetShoot", timeBetweenShooting);

        //ako imamo metaka cekaj funckiju shoot sa definiranim vremenskim razmakom, npr na automaskoj puski nam je malo a na pistolju malo vise od pola sekunde
        if (bulletsShot > 0 && bulletsLeft > 0)
        {
            Invoke("shoot", timeBetweenShooting);
        }
    }

    private void resetShoot()
    {
        readyToShoot = true;
    }

    private void reload()
    {
        reloading = true;

        //pusti zvuk za reload
        reloadSounds.Play();

        Invoke("reloadFinished", reloadTime);

    }

    //funckcija koja provjerava metke u san�eru i koliko ih imamo u inventory-u
    private void reloadFinished()
    {
        if (bulletsLeft != magazineSize)
        {
            int firedAmmo = magazineSize - bulletsLeft;
            ammo -= firedAmmo;
            bulletsLeft += firedAmmo;
        }

        int count = bulletsLeft + ammo;

        if (count < magazineSize)
        {
            bulletsLeft = count;
            ammo = 0;
        }

        if (ammo <= 0)
        {
            ammo = 0;
        }

        reloading = false;
    }

}