using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBreaker : MonoBehaviour
{
    public int door_health;
    public GameObject door;
    private GameObject player;

    private bool triggeringWithDoor;
    private bool soundAlreadyPlayed = false;

    public AudioSource source;
    public AudioClip doorHit, doorBreak;


    private void Update()
    {

        if (triggeringWithDoor)
        {
            if (player.GetComponent<Player>().weaponEquipped)
            {
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    source.PlayOneShot(doorHit);
                    door_health -= 5;
                }
            }
        }

        if (door_health <= 0)
        {
            if (!soundAlreadyPlayed)
            {
                source.PlayOneShot(doorBreak);
                soundAlreadyPlayed = true;
            }

            Destroy(door);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            //print(triggeringWithDoor);
            triggeringWithDoor = true;
            player = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //print(triggeringWithDoor);
        triggeringWithDoor = false;
    }








}
